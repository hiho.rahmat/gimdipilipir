extends Popup

onready var state = 1
onready var texture = get_node("TextureRect")
onready var next = get_node("TextureRect/Next")


func _process(delta):
	display()
	show()
	
func show():
	if global.showTutorial:
		get_tree().paused = true
		popup()
	else:
		get_tree().paused = false
		hide()

func display():
	texture.set_texture(load("res://Assets/GUI/Nonogram_Tutorial/tutorial_"+str(state)+".png"))
	if state == 3 :
		next.set_pressed_texture(load("res://Assets/GUI/Nonogram_Tutorial/Pecahkan_Pressed.png"))
	else:
		next.set_pressed_texture(load("res://Assets/GUI/Nonogram_Tutorial/Selanjutnya_Pressed.png"))

func _on_X_pressed():
	global.showTutorial = false
	state = 1


func _on_Next_pressed():
	#print(str(state))
	if state==3:
		global.showTutorial = false
		state=1
		
	else:
		state +=1

