extends Popup

export(String) onready var type 
var isInInventory = false


func _process(delta):
	checkInventory()
	checkstate()


func checkInventory():
	if global.inventory.has(self.type):
		isInInventory = true
	else:
		isInInventory = false

func checkstate():
	if isInInventory && global.isOpenInventory:
		popup()
	else:
		hide()
