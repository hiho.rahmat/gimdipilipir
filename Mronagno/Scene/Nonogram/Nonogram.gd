extends Popup

export(int) onready var nonogramNumber
export(String,"Jembatan","Batu","Ikan","Pintu") onready var type
onready var result = []
onready var display = get_node("TextureRect/Display")
onready var benar = load("res://Assets/GUI/Nonogram/Result_Benar.png")
onready var salah = load("res://Assets/GUI/Nonogram/Result_Salah.png")
onready var audio = get_node("AudioStreamPlayer")
onready var audioSalah = load("res://Assets/Sound/SFX/Wrong.wav")
onready var audioBenar = load("res://Assets/Sound/SFX/Correct.wav")
onready var audioClose = load("res://Assets/Sound/SFX/Button Press - Back.wav")
var sfxDone

onready var current = [false,false,false,false,false,
					false,false,false,false,false,
					false,false,false,false,false,
					false,false,false,false,false,
					false,false,false,false,false]

func _ready():
	#popup()
	loadData()
	loadSound()

func loadSound():
	match type:
		"Jembatan":
			sfxDone = load("res://Assets/Sound/SFX/Hammering.wav")
		"Batu":
			sfxDone = load("res://Assets/Sound/SFX/Rock_Destroyed.wav")
		"Ikan":
			sfxDone = load("res://Assets/Sound/SFX/Item_Get.wav")
		"Pintu":
			sfxDone = load("res://Assets/Sound/SFX/Lock_Open.wav")

func loadData():
	$TextureRect.set_texture(load("res://Assets/GUI/Nonogram/Nonogram_"+str(nonogramNumber)+"_Base.png"))
	match nonogramNumber:
		1:
			result=global.nonogram1
		2:
			result=global.nonogram2
		3:
			result=global.nonogram3
		4:
			result=global.nonogram4
		5:
			result=global.nonogram5
		6:
			result=global.nonogram6


func changeCurrentState(position,value):
	current[position] = value


func getResult():
	loadData()
	return result


func _on_CheckButton_pressed():
	var i = 0
	while i<current.size():
		if current[i] == false:
			display.set_texture(salah)
			audio.set_stream(audioSalah)
			audio.play()
			return
		i+=1
	display.set_texture(benar)
	audio.set_stream(audioBenar)
	audio.play()
	if !global.nonogramDone.has("Nonogram"+str(nonogramNumber)):
		global.nonogramDone.push_back("Nonogram"+str(nonogramNumber))
		print(str(global.nonogramDone))
	yield(audio,"finished")
	$AnimationPlayer.play("Fade_In")
	yield($AnimationPlayer,"animation_finished")
	hide()
	dialogControl.isSpeaking = true
	audio.set_stream(sfxDone)
	audio.play()
	yield(audio,"finished")
	$AnimationPlayer.play("Fade_Out")
	yield($AnimationPlayer,"animation_finished")
	dialogControl.isSpeaking = false
	queue_free()


func _on_Close_pressed():
	audio.set_stream(audioClose)
	audio.play()
	global.isNonogramActive = false
	hide()


func _on_Nonogram_about_to_show():
	global.isNonogramActive = true


func _on_Nonogram_popup_hide():
	global.isNonogramActive = false
