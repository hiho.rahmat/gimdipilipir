extends Area2D

var state = false
export(int) onready var x
export(int) onready var y
export(int) onready var nonogramNumber

var player

func _process(delta):
	isActive()
	if state && Input.is_action_just_pressed("ui_accept"):
		teleport()

func isActive():
	if global.nonogramDone.has("Nonogram"+str(nonogramNumber)):
		$CollisionShape2D.set_disabled(false)
		$Sprite.show()
	else:
		$CollisionShape2D.set_disabled(true)
		$Sprite.hide()

func teleport():
		dialogControl.isSpeaking = true
		$CanvasLayer/AnimationPlayer.play("Fade_In")
		yield($CanvasLayer/AnimationPlayer,"animation_finished")
		player.set_position(Vector2(x,y)) 
		$CanvasLayer/AudioStreamPlayer.play()
		yield($CanvasLayer/AudioStreamPlayer,"finished")
		$CanvasLayer/AnimationPlayer.play("Fade_Out")
		yield($CanvasLayer/AnimationPlayer,"animation_finished")
		dialogControl.isSpeaking = false
		
func _on_Teleport_body_entered(body):
	if(body.get_name() == "Player"):
		player = body
		state = true
		global.canInteract = true


func _on_Teleport_body_exited(body):
	if(body.get_name() == "Player"):
		state = false
		global.canInteract = false
