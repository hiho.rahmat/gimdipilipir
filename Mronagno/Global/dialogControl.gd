extends Node

var npcState = {
	"NPC":"Introduction" ,
	"Pak Tua" : "Introduction",
	"Kepala Desa" : "Introduction",
	"Petani" : "Introduction",
	"Pedagang" : "Introduction",
	"Pemancing" : "Introduction",
	"Nenek" : "Introduction",
	"Anak" : "Repetitive 1",
	"Ibu" : "Repetitive 1",
	"Penambang" : "Introduction",
	"Test" : "Quest Start",
	"Test Kades" : "Introduction",
}

var isSpeaking = false

var dialog_path = "res://Dialog/Dialog.json"
var cutscene_path = "res://Dialog/Cutscene.json"
var dialog = load_dialog_from_json(dialog_path)
var cutscene = load_dialog_from_json(cutscene_path)


func change_npc_state(type,state):
	npcState[type]=state


func load_dialog_from_json(file_path) -> Dictionary:
	var file = File.new()
	#assert(file.file_exists(file_path)) 
	file.open(file_path, file.READ)
	var dialog = parse_json(file.get_as_text())
	#assert(dialog.size() > 0)
	file.close()
	return dialog
	
