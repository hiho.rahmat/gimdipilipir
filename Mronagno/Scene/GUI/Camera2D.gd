extends Camera2D

onready var tween = get_node("Tween")

func move(target):
	tween.interpolate_property(self,"position",position,target,3,Tween.TRANS_QUINT,Tween.EASE_OUT)
	tween.start()
	
func back():
	tween.interpolate_property(self,"position",position,Vector2(0,0),3,Tween.TRANS_QUINT,Tween.EASE_OUT)
	tween.start()
