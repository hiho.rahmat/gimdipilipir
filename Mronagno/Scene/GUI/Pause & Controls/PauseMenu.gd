extends Popup


onready var canBePressed = true
onready var kontrol = get_node("../Kontrol")
onready var tutorial = get_node("../TutorialNonogram")


func _process(delta):
	canActivate()
	close()
	if canBePressed && Input.is_action_just_pressed("ui_cancel"):
		global.showPauseMenu = true
	if global.showPauseMenu:
		get_tree().paused = true
		popup()
	else:
		get_tree().paused = false
		hide()



func canActivate():
	if global.isOpenInventory:
		canBePressed = false
	elif global.isNonogramActive:
		canBePressed = false
	elif global.showControl:
		canBePressed = false
	elif global.showTutorial:
		canBePressed = false
	elif dialogControl.isSpeaking:
		canBePressed = false
	elif global.showPauseMenu:
		canBePressed = false
	elif global.showTutorial:
		canBePressed = false
	else:
		canBePressed = true

func close():
	if is_visible_in_tree()&&Input.is_action_just_pressed("ui_cancel"):
		get_tree().paused = false
		hide()
		global.showPauseMenu =false
		
func _on_Btn_Lanjutkan_pressed():
	global.showPauseMenu = false


func _on_Btn_Keluar_pressed():
	get_tree().quit()


func _on_Btn_Kontrol_pressed():
	global.showPauseMenu = false
	global.showControl = true

func _on_Btn_Tutorial_pressed():
	global.showPauseMenu = false
	global.showTutorial=true
