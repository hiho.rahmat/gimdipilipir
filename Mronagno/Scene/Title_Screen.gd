extends Control



export(String) onready var changeTo 



func _on_Mainkan_pressed():
	$Background/Mainkan/ClickSFX.play()
	$AnimationPlayer.play("Fade_to_Black")
	yield($AnimationPlayer,"animation_finished")
	$AnimationPlayer.play("Sound Fading")
	yield($AnimationPlayer,"animation_finished")
	get_tree().change_scene(str("res://Scene/"+changeTo+".tscn"))

