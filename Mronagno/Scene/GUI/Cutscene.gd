extends Area2D

onready var dBox = get_node("/root/World/CanvasLayer/DBox")
onready var camera = get_node("../../YSort/Player/Camera2D")
export(String) onready var cutscene_name 
export(int) onready var camera_x 
export(int) onready var camera_y 


onready var cutscene = dialogControl.cutscene[cutscene_name]
var text_index = 0
var cutscenePlay = false
onready var camera_target = Vector2(camera_x,camera_y)

func _process(delta):
	if cutscenePlay && Input.is_action_just_pressed("ui_accept"):
		play()
	

func play():
	var text = cutscene
	if text_index < text.size():
		dBox.name_set(text[str(text_index)]["name"])
		dBox.dialog_set(text[str(text_index)]["text"])
		dBox.open()
		text_index+=1
	else:
		dBox.close()
		text_index = 0
		camera.back()
		if cutscene_name == "Depan Pintu Rumah Tabib":
			global.canSolveTabib = true
		elif cutscene_name == "Final Scene":
			global.isTabibDone = true
		queue_free()


func _on_Cutscene_area_entered(area):
	camera.move(camera_target)
	play()
	cutscenePlay = true


func _on_Cutscene_area_exited(area):
	camera.back()
	queue_free()
