extends Sprite

onready var dBox = get_node("/root/World/CanvasLayer/DBox")
export(String) onready var npc_name 
export(String) onready var quest_item  


var canTalk = false
onready var npc_state = dialogControl.npcState[npc_name]
onready var dialog = dialogControl.dialog[npc_name]
var text_index = 0



	
func _process(delta):
	isActive()
	isItemFound()
	isTalkedToKades()
	questComplete()
	npc_state = dialogControl.npcState[npc_name]
	
	if canTalk && Input.is_action_just_pressed("ui_accept"):
		talk()



func talk():
	var text = dialog[npc_state]
	if text_index < text.size():
		dBox.name_set(text[str(text_index)]["name"])
		dBox.dialog_set(text[str(text_index)]["text"])
		dBox.open()
		text_index+=1
	else:
		dBox.close()
		text_index = 0
		advanceState()


func advanceState():
	match npc_state:
		"Introduction":
			dialogControl.change_npc_state(npc_name,"Repetitive 1")
		"Repetitive 1":
			pass
		"Quest Start":
			global.activeItems.push_back(quest_item)
			dialogControl.change_npc_state(npc_name,"Quest Check") 
		"Quest Check":
			pass
		"Quest Finished":
			if global.isKaDesDone:
				dialogControl.change_npc_state(npc_name,"Repetitive 2") 
			pass
		"Repetitive 2":
			pass

func isTalkedToKades():
	if global.isTalkedToKaDes:
		if npc_state == "Repetitive 1":
			dialogControl.change_npc_state(npc_name,"Quest Start")
			
func questComplete():
	if npc_state == "Quest Finished" && global.isKaDesDone:
		dialogControl.change_npc_state(npc_name,"Repetitive 2")
		


func isItemFound():
	if global.inventory.has(quest_item) && npc_state=="Quest Check":
		dialogControl.change_npc_state(npc_name,"Quest Finished")

func isActive():
	if global.nonogramDone.has("Nonogram5"):
		queue_free()

func _on_InteractableArea_area_entered(area):
	canTalk = true
	global.canInteract = true


func _on_InteractableArea_area_exited(area):
	canTalk = false
	global.canInteract = false
