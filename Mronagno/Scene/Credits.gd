extends Node2D
var canExit

func _ready():
	canExit = false
	$Fade/ColorRect.show()
	$Text/Exit.hide()
	animate()

func _process(delta):
	if canExit&&Input.is_action_just_pressed("ui_accept"):
		get_tree().quit()
		
func animate():
	$AnimationPlayer.play("Fade_In")
	yield($AnimationPlayer,"animation_finished")
	$AnimationPlayer.play("Credit")
	yield($AnimationPlayer,"animation_finished")
	canExit = true
	$Text/Exit.show()
