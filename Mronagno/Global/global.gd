extends Node

var BGMLevel = 1
var inventory = []
var activeItems = []
var isOpenInventory = false
var questDone = []
var isTalkedToKaDes = false
var isKaDesDone = false
var isTalkedToPenambang = false
var isPenambangDone = false
var isNonogramActive = false
var nonogramDone = []
var canInteract = false
var showTutorialPertama = false
var showTutorial = false 
var showControl = false
var showPauseMenu = false
var canSolveTabib = false
var isTabibDone = false


var nonogram1 = [false,true,false,true,false,
				true,true,true,true,true,
				true,true,true,true,true,
				false,true,true,true,false,
				false,false,true,false,false]

var nonogram2 = [true,false,true,false,true,
				true,true,true,true,true,
				false,true,true,true,false,
				false,true,false,true,false,
				false,true,true,true,false]

var nonogram3 = [false,false,false,true,true,
				false,false,true,true,true,
				false,true,true,false,true,
				true,true,false,false,true,
				true,true,true,true,true]

var nonogram4 = [true,true,true,true,true,
				false,true,true,true,false,
				false,false,true,false,false,
				false,true,false,true,false,
				true,true,true,true,true]

var nonogram5 = [false,true,true,false,false,
				false,false,true,false,false,
				true,true,true,true,true,
				true,false,true,false,true,
				true,true,true,false,false]

var nonogram6 = [false,false,true,true,false,
				false,true,true,true,false,
				true,true,true,false,false,
				true,true,true,false,true,
				false,false,false,false,true]
