extends Node2D

onready var player = get_node("../../YSort/Player")

func _process(delta):
	if global.isTabibDone:
		end()
		global.isTabibDone = false

func end():
	dialogControl.isSpeaking = true
	$AnimationPlayer.play("Fade_In")
	yield($AnimationPlayer,"animation_finished")
	$AnimationPlayer.play("BGM_off")
	yield($AnimationPlayer,"animation_finished")
	$AnimationPlayer.play("Walkout")
	player.set_position(Vector2(1533.102,-595.643))
	yield($AnimationPlayer,"animation_finished")
	$AnimationPlayer.play("Fade_Out")
	yield($AnimationPlayer,"animation_finished")
	$AnimationPlayer.play("Camera")
	yield($AnimationPlayer,"animation_finished")
	get_tree().change_scene("res://Scene/Credits.tscn")
	



