extends AudioStreamPlayer


onready var BGMLevel = global.BGMLevel

func _ready():
	$AnimationPlayer.play("Sound_In")
	
func _process(delta):
	if BGMLevel != global.BGMLevel:
		BGMLevel=global.BGMLevel
		print(BGMLevel)
		$AnimationPlayer.play("Sound_Out")
		yield($AnimationPlayer,"animation_finished")
		loadNewBgm()
		$AnimationPlayer.play("Sound_In")
		yield($AnimationPlayer,"animation_finished")

func loadNewBgm():
	match global.BGMLevel:
		1:
			set_stream(load("res://Assets/Sound/BGM/Windless Slopes.ogg"))
		2:
			set_stream(load("res://Assets/Sound/BGM/Windless Slopes.ogg"))
		3:
			set_stream(load("res://Assets/Sound/BGM/Midori no Kaze.ogg"))
	play()
	
