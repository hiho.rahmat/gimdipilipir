extends Popup

func _ready():
	hide()


func _process(delta):
	if global.canInteract && !dialogControl.isSpeaking && !global.isNonogramActive:
		popup()
	else:
		hide()
