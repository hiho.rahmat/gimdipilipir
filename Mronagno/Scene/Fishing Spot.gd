extends Area2D

export(String) onready var type
export(int) onready var nonogramNumber
var nonogram
var canFish = false

func _process(delta):
	isActive()
	isFinishedFishing()
	if canFish && Input.is_action_just_pressed("ui_accept"):
		nonogram.popup()
		global.isNonogramActive = true

func _ready():
	isActive()
	nonogram = get_node("../../../CanvasLayer/Nonogram"+str(nonogramNumber))

func isFinishedFishing():
	if global.nonogramDone.has("Nonogram"+str(nonogramNumber)):
		global.inventory.push_back(type)
		queue_free()

func isActive():
	if global.isTalkedToPenambang:
		$CollisionShape2D.set_disabled(false)
		$Sprite.show()
	else:
		$CollisionShape2D.set_disabled(true)
		$Sprite.hide()


func _on_Fishing_Spot_1_body_entered(body):
	if body.get_name()=="Player":
		global.canInteract = true
		canFish = true



func _on_Fishing_Spot_1_body_exited(body):
	if body.get_name()=="Player":
		global.canInteract = false
		canFish = false
