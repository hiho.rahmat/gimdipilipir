extends Sprite

onready var dBox = get_node("/root/World/CanvasLayer/DBox")
export(String) onready var npc_name 
export(Array,String) onready var quest_item  


var canTalk = false
onready var npc_state = dialogControl.npcState[npc_name]
onready var dialog = dialogControl.dialog[npc_name]
var text_index = 0



	
func _process(delta):
	isItemFound()
	checkBubble()
	npc_state = dialogControl.npcState[npc_name]
	
	if canTalk && Input.is_action_just_pressed("ui_accept"):
		talk()



func talk():
	var text = dialog[npc_state]
	if text_index < text.size():
		dBox.name_set(text[str(text_index)]["name"])
		dBox.dialog_set(text[str(text_index)]["text"])
		dBox.open()
		text_index+=1
	else:
		dBox.close()
		text_index = 0
		advanceState()


func advanceState():
	match npc_state:
		"Introduction":
			dialogControl.change_npc_state(npc_name,"Quest Check")
			global.isTalkedToPenambang = true
			activateItems()
		"Repetitive 1":
			pass
		"Quest Start":
			pass
		"Quest Check":
			pass
		"Quest Finished":
			#print("q f")
			dialogControl.change_npc_state(npc_name,"Repetitive 2") 
			questComplete()
		"Repetitive 2":
			pass

func activateItems():
	var i = 0
	while i<quest_item.size():
		if !global.activeItems.has(quest_item[i]):
			global.activeItems.push_back(quest_item[i])
		i+=1

func questComplete():
	removeItems()
	global.questDone.append(npc_name)
	global.isPenambangDone=true
	#print(global.isPenambangDone)
	#print("quest done : "+str(global.questDone))
		

func checkItems():
	var i = 0
	while i<quest_item.size():
		if !global.inventory.has(quest_item[i]):
			return false
		i+=1
	return true

func isItemFound():
	if checkItems() && npc_state=="Quest Check":
		dialogControl.change_npc_state(npc_name,"Quest Finished")

func removeItems():
	var i = 0
	while i<quest_item.size():
		global.inventory.erase(quest_item[i])
		i+=1

func checkBubble():
	match npc_state:
		"Introduction":
			$"Emote Bubble/AnimationPlayer".play("Available")
		"Quest Check":
			$"Emote Bubble/AnimationPlayer".play("Ongoing")
		"Quest Finished":
			$"Emote Bubble/AnimationPlayer".play("Done")
		"Repetitive 2":
			$"Emote Bubble".hide()

func _on_InteractableArea_area_entered(area):
	canTalk = true
	global.canInteract = true


func _on_InteractableArea_area_exited(area):
	canTalk = false
	global.canInteract = false
