extends TextureButton

onready var default = null
onready var black = load("res://Assets/GUI/Nonogram/Black.png")
onready var silang = load("res://Assets/GUI/Nonogram/X.png")
onready var isBlack = false
onready var isSilang = false
onready var display = default
onready var nonogram = get_node("../../../..")
onready var nonogramResult = nonogram.getResult()
export(int) onready var buttonNumber
onready var supposedValue = nonogramResult[buttonNumber]
onready var value = false

func _ready():
	self.texture_normal = default


func _process(delta):
	checkDisplay()
	checkValue()
	self.texture_normal = display

func checkDisplay():
	if isSilang:
		display = silang
	elif isBlack :
		display = black
	else:
		display = default
		

func checkValue():
	if value == supposedValue:
		nonogram.changeCurrentState(buttonNumber,true)
	else:
		nonogram.changeCurrentState(buttonNumber,false)

func _on_ButtonNonogram_gui_input(event):
	if event is InputEventMouseButton and event.pressed:
		match event.button_index:
			BUTTON_LEFT:
				isSilang = false
				value = !value
				isBlack = !isBlack
				checkValue()
			BUTTON_RIGHT:
				isBlack = false
				value = false
				isSilang = !isSilang
				checkValue()
