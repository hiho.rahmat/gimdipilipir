extends Popup

func _ready():
	global.showControl = true
	
func _process(delta):
	if global.showControl:
		popup()
		get_tree().paused = true
	else:
		hide()
		get_tree().paused = false


func _on_Tutup_pressed():
	get_tree().paused = false
	global.showControl = false
	hide()
