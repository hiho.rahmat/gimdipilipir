extends Popup


func _process(delta):
	if dialogControl.isSpeaking:
		global.isOpenInventory = false
		hide()
	if Input.is_action_just_pressed("ui_inventory"):
		changeState()



func changeState():
	if global.isOpenInventory or dialogControl.isSpeaking:
		global.isOpenInventory = false
		hide()
	else:
		global.isOpenInventory = true
		popup()


func _on_Inventory_about_to_show():
	$OpenSFX.play()


func _on_Inventory_popup_hide():
	$CloseSFX.play()
