extends Area2D

export(int) onready var nonogramNumber
var canInteract 
var nonogram

func _ready():
	canInteract = false
	nonogram = get_node("../../../CanvasLayer/Nonogram"+str(nonogramNumber))

func _process(delta):
	isActive()
	isFinished()
	if canInteract && Input.is_action_just_pressed("ui_accept"):
		interact()

func isActive():
	if (global.isTalkedToPenambang && nonogramNumber==2) || global.canSolveTabib&&nonogramNumber==6:
		$CollisionShape2D.set_disabled(false)
	else:
		$CollisionShape2D.set_disabled(true)

func interact():
	nonogram.popup()
	global.isNonogramActive = true

func isFinished():
	if global.nonogramDone.has("Nonogram"+str(nonogramNumber)):
		queue_free()

func _on_Locked_Door1_body_entered(body):
	if body.get_name()=="Player":
		global.canInteract = true
		canInteract = true


func _on_Locked_Door1_body_exited(body):
	if body.get_name()=="Player":
		global.canInteract = false
		canInteract = false
