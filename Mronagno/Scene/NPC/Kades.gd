extends Sprite

onready var dBox = get_node("/root/World/CanvasLayer/DBox")
export(String) onready var npc_name 
export(Array,String) onready var quest_complete_requirements 


var canTalk = false
onready var npc_state = dialogControl.npcState[npc_name]
onready var dialog = dialogControl.dialog[npc_name]
var text_index = 0



	
func _process(delta):
	checkBubble()
	isQuestComplete()
	npc_state = dialogControl.npcState[npc_name]
	if canTalk && Input.is_action_just_pressed("ui_accept"):
		talk()



func talk():
	var text = dialog[npc_state]
	if text_index < text.size():
		dBox.name_set(text[str(text_index)]["name"])
		dBox.dialog_set(text[str(text_index)]["text"])
		dBox.open()
		text_index+=1
	else:
		dBox.close()
		text_index = 0
		advanceState()


func advanceState():
	match npc_state:
		"Introduction":
			dialogControl.change_npc_state(npc_name,"Quest Check")
			global.isTalkedToKaDes = true
		"Quest Check":
			pass
		"Quest Finished":
			dialogControl.change_npc_state(npc_name,"Repetitive 2")
			global.isKaDesDone = true 
			removeItems()
		"Repetitive 2":
			pass

func isQuestComplete():
	if checkItems() && npc_state=="Quest Check":
		dialogControl.change_npc_state(npc_name,"Quest Finished")

func removeItems():
	var i = 0
	while i<quest_complete_requirements.size():
		global.inventory.erase(quest_complete_requirements[i])
		i+=1


func checkItems():
	var i = 0
	while i<quest_complete_requirements.size():
		if !global.inventory.has(quest_complete_requirements[i]):
			return false
		i+=1
	return true
	
func checkBubble():
	match npc_state:
		"Introduction":
			$"Emote Bubble/AnimationPlayer".play("Available")
		"Quest Check":
			$"Emote Bubble/AnimationPlayer".play("Ongoing")
		"Quest Finished":
			$"Emote Bubble/AnimationPlayer".play("Done")
		"Repetitive 2":
			$"Emote Bubble".hide()


func _on_InteractableArea_area_entered(area):
	canTalk = true
	global.canInteract = true


func _on_InteractableArea_area_exited(area):
	canTalk = false
	global.canInteract = false
