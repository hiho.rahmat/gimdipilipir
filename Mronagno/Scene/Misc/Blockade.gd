extends Sprite

onready var canInteract = false
export(int) onready var nonogramNumber
onready var dialog = dialogControl.cutscene["Nonogram "+str(nonogramNumber)]
var nonogram 
onready var dialogPlay = false
onready var dBox = get_node("../../../CanvasLayer/DBox")
var text_index = 0
var isActive



func _ready():
	nonogram = get_node("../../../CanvasLayer/Nonogram"+str(nonogramNumber))

func _process(delta):
	connectToGlobal()
	isExist()
	if canInteract && Input.is_action_just_pressed("ui_accept"):
		interact()

func interact():
	if isActive:
		if global.showTutorialPertama:
			nonogram.popup()
			global.isNonogramActive = true
		else:
			global.showTutorial = true
			global.showTutorialPertama = true
	else :
		startDialog()

func connectToGlobal():
	match nonogramNumber:
		1:
			isActive=global.isKaDesDone
		5:
			isActive=global.isPenambangDone

func startDialog():
	var text = dialog
	if text_index < text.size():
		dBox.name_set(text[str(text_index)]["name"])
		dBox.dialog_set(text[str(text_index)]["text"])
		dBox.open()
		text_index+=1
	else:
		dBox.close()
		text_index = 0
	
	
func isExist():
	if global.nonogramDone.has("Nonogram"+str(nonogramNumber)):
		queue_free()
	
func _on_Area2D_area_entered(area):
	canInteract = true
	global.canInteract = true
	if !isActive:
		dialogPlay = true
		startDialog()


func _on_Area2D_area_exited(area):
	canInteract = false
	global.canInteract = false
	dialogPlay = false
