extends Sprite

onready var dBox = get_node("/root/World/CanvasLayer/DBox")
export(String) onready var npc_name 



var canTalk = false
onready var npc_state = "Introduction"
onready var dialog = dialogControl.dialog[npc_name]
var text_index = 0



	
func _process(delta):
	npc_state = dialogControl.npcState[npc_name]
	if canTalk && Input.is_action_just_pressed("ui_accept"):
		talk()



func talk():
	var text = dialog[npc_state]
	if text_index < text.size():
		dBox.name_set(text[str(text_index)]["name"])
		dBox.dialog_set(text[str(text_index)]["text"])
		dBox.open()
		text_index+=1
	else:
		dBox.close()
		text_index = 0
		advanceState()


func advanceState():
	if npc_state == "Introduction":
		dialogControl.change_npc_state(npc_name,"Repetitive 1")



func _on_InteractableArea_area_entered(area):
	canTalk = true
	global.canInteract = true

func _on_InteractableArea_area_exited(area):
	canTalk = false
	global.canInteract = false
