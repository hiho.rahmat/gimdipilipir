extends Popup

var speaker_name setget name_set
var dialog setget dialog_set

func name_set(new_name):
	$TectureRect/Speaker.bbcode_text="" #clear karena kalo engga, bakal nambah trus gara2 add_text
	speaker_name = new_name
	$TectureRect/Speaker.bbcode_text="[center]"+speaker_name+"[/center]"

func dialog_set(new_dialog):
	$TectureRect/Dialog.bbcode_text="" #clear karena kalo engga, bakal nambah trus gara2 add_text
	dialog = new_dialog
	$TectureRect/Dialog.bbcode_text = dialog


func _ready():
	set_process_input(false)


func open():
	dialogControl.isSpeaking = true
	#get_tree().paused = true
	$TectureRect/Dialog.percent_visible = 0
	$AnimationPlayer.playback_speed = 60.0 / dialog.length()
	$AnimationPlayer.play("ShowDialog")
	$NextDialogSFX.play()
	popup()
	

func close():
	dialogControl.isSpeaking = false
	#get_tree().paused = false
	hide()
