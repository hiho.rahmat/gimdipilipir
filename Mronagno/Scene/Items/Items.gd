extends Sprite

export(String) onready var type 
onready var body = get_node("StaticBody2D")
var canPickup = false

func _ready():
	isActive()
	#print(str(global.activeItems))

func _process(delta):
	isActive()
	if canPickup && Input.is_action_just_pressed("ui_accept"):
		pickup()
	
func isActive():
	if global.activeItems.has(type):
		show()
		#body.set_process(true)
		#body.set_physics_process(true)
		#body.set_process_input(true)
		$StaticBody2D/CollisionShape2D.set_disabled(false)
		$StaticBody2D/InteractableArea/CollisionShape2D.set_disabled(false)
	else:
		hide()
		#body.set_process(false)
		#body.set_physics_process(false)
		#body.set_process_input(false)
		$StaticBody2D/CollisionShape2D.set_disabled(true)
		$StaticBody2D/InteractableArea/CollisionShape2D.set_disabled(true)

func pickupSFX():
	$PickupSFX.play()
	print("ikan SFX")
		
	
func pickup():
	$PickupSFX.play()
	global.activeItems.erase(type)
	self.hide()
	yield( $PickupSFX, "finished" )
	global.inventory.push_back(type)
	queue_free()


func _on_InteractableArea_area_entered(area):
	canPickup = true
	global.canInteract = true


func _on_InteractableArea_area_exited(area):
	canPickup = false
	global.canInteract = false
